# controller.py

# required libraries
import tkinter as tk
from tkinter import font as tkfont
from view import *
from model import *


class Controller(tk.Tk):
    # class variable to hold currently logged in user object
    user = None

    # constructor
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        # defining font for titles
        self.title_font = tkfont.Font(family='Helvetica', size=25, weight="bold", slant="italic")

        # defining grid container for the views
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # initialising all of the frames
        self.frames = {}
        for F in (Login, Dashboard, Modules, Logout, Assessments, Activities, Tasks):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            # defining the modal screen container so that it doesn't cover the whole page
            if page_name == "Activities" or page_name == "Tasks":
                frame.grid(row=0, column=0, sticky="nsew", pady=(80, 80), padx=(300, 300))
            else:
                frame.grid(row=0, column=0, sticky="nsew", pady=(50, 50), padx=(50, 50))

        # show opening screen 'login'
        self.show_frame("Login")

    # method allowing user to login
    def login(self, username, password):
        data = User.get_user(username, password)
        if data != -1:
            self.user = data
            self.show_frame("Dashboard", data)
        else:
            self.frames["Login"].login_failed()

    # allow user logout, show login page again
    def logout(self, previous_frame):
        self.frames["Logout"].return_to(previous_frame)
        self.show_frame("Logout")

    # loading in user module details
    def module_details(self, module_id):
        frame = self.frames["Modules"]
        frame.title(Module.get_module(module_id).name)
        data = Assessment.get_assessments(module_id)
        self.show_frame("Modules", data)

    # loading in user assessment details
    def assessment_details(self, assessment_id):
        Assessment.get_assessment(assessment_id)
        frame = self.frames["Assessments"]
        frame.assessment_id = assessment_id
        frame.title(Assessment.get_assessment(assessment_id).name)
        data = Task.get_tasks(assessment_id, self.user)
        self.show_frame("Assessments", data)

    # loading in user activity details
    def activities(self, data, task_id):
        self.frames["Assessments"].dim()
        for d in data:
            if d.id == task_id:
                data = d
                break
        self.frames["Activities"].task_id = task_id
        self.show_frame("Activities", data)

    # loading in user activity details
    def tasks(self, assessment_id):
        self.frames["Assessments"].dim()
        self.frames["Tasks"].assessment_id = assessment_id
        self.show_frame("Tasks")

    # method to facilitate task addition
    def add_task(self, task):
        task.id = Task.next_id()
        Task.add_task(task)
        self.frames["Assessments"].undim()

    # method to facilitate activity addition
    def add_activity(self, activity):
        activity.id = Activity.next_id()
        Activity.add_activity(activity)
        self.frames["Assessments"].undim()

    # method to facilitate activity removal
    def remove_activity(self, activity):
        pass

    # method to facilitate task removal
    def remove_task(self, task):
        Task.remove_task(task.id)
        self.show_frame("Tasks")

    # method to raise frame so that it is visible
    def show_frame(self, page_name, data=None):
        frame = self.frames[page_name]
        if page_name == "Dashboard":
            data = self.user
        frame.user_data(data)
        frame.tkraise()


# main() function to create the tkinter app and enter the mainloop
if __name__ == "__main__":
    app = Controller()
    app.title("Study Planner")
    app.geometry("1000x600")
    app.resizable(0, 0)
    app.mainloop()
