# view.py

# all necessary libraries
import tkinter as tk
from tkinter import ttk
from model import *
from tkinter import PhotoImage, RIGHT, LEFT
from tkinter.scrolledtext import ScrolledText
from operator import attrgetter

# setting useful module information
ROOT = os.path.dirname(__file__)
colours = "red", "green", "blue", "orange", "pink", "purple", "yellow"


# disassociate class tags with instances of a class (for adding and removing binds)
def unbind_tags(instance):
    bindtags = list(instance.bindtags())
    for b in range(1, len(bindtags)):
        bindtags.remove(list(instance.bindtags())[b])
    instance.bindtags((tuple(bindtags)))


# drawing the login view
class Login(tk.Frame):
    # construction
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.lbl_failed = None

        # drawing the tkinter modules which make up the view
        lbl_title = tk.Label(self, text="Login", font=controller.title_font)
        lbl_title.pack(side="top", fill="x", pady=(0, 5))

        lbl_username = tk.Label(self, text="Username")
        ent_username = tk.Entry(self)
        lbl_username.pack()
        ent_username.pack()
        ent_username.focus()

        lbl_password = tk.Label(self, text="Password")
        ent_password = tk.Entry(self, show="*")     # ensures the password is not exposed
        lbl_password.pack()
        ent_password.pack()

        # tries to login, if it doesnt work, acts accordingly
        def login_callback():
            self.lbl_failed.pack_forget()
            controller.login(ent_username.get(), ent_password.get())
            ent_password.delete(0, "end")
            ent_username.delete(0, "end")
            ent_username.focus()

        btn_login = tk.Button(self, text="Log in",
                              command=lambda: login_callback())
        btn_login.pack(pady=(10, 0))

        ent_username.bind("<Return>", lambda e: login_callback())
        ent_password.bind("<Return>", lambda e: login_callback())

        self.lbl_failed = tk.Label(self, text="Incorrect Credentials", fg="red")

    # displays 'incorrect credentials'
    def login_failed(self):
        self.lbl_failed.pack_forget()
        self.lbl_failed.pack()

    # fill the frame with user specific data (called every time data is changed or this view is raised) (unused here)
    def user_data(self, data=None):
        pass


# drawing the logout view
class Logout(tk.Frame):
    # initialisation
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller

        # drawing the tkinter modules which make up the view
        lbl_title = tk.Label(self, text="Logout?", font=controller.title_font)
        lbl_title.pack(side="top", fill="x", pady=(0, 5))

        btn_yes = tk.Button(self, text="Logout",
                            command=lambda: controller.show_frame("Login"))
        btn_yes.pack(pady=(0, 300), padx=(0, 300), side=RIGHT)

        self.btn_no = tk.Button(self, text="Cancel",
                                command=lambda: controller.show_frame("Dashboard"))
        self.btn_no.pack(pady=(0, 300), padx=(300, 0), side=LEFT)

    # returns to the view where logout was called if the user cancels
    def return_to(self, page_name):
        self.btn_no.config(command=lambda: self.controller.show_frame(page_name))

    # fill the frame with user specific data (called every time data is changed or this view is raised) (unused here)
    def user_data(self, data):
        pass


# drawing the dashboard view
class Dashboard(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

        # drawing the tkinter modules which make up the view
        lbl_title = tk.Label(self, text="Dashboard", font=controller.title_font)
        lbl_title.grid(column=1, sticky="nsew")

        btn_reload = tk.Button(self, text="Reload", width=6,
                               command=lambda: controller.show_frame("Dashboard"))
        btn_reload.grid(row=0, sticky="nw")

        self.frm_modules = tk.Frame(self)
        self.frm_modules.grid(row=1, column=1, sticky="nsew")
        self.frm_modules.columnconfigure(1, weight=1)
        self.frm_modules.rowconfigure(1, weight=1)

        self.frm_modulelist = ScrollFrame(self.frm_modules, 210)
        self.frm_modulelist.grid(row=1, column=0, sticky="nsew")

        self.frm_pane = tk.Frame(self.frm_modules, highlightbackground="black",
                                 highlightcolor="black", highlightthickness=1)
        self.frm_pane.grid(row=1, column=1, sticky="nsew")
        self.frm_pane.rowconfigure(2, weight=1)
        self.frm_pane.columnconfigure(0, weight=1)
        self.frm_pane.columnconfigure(1, weight=1)

        self.lbl_pane_title = tk.Label(self.frm_pane, font="Helvetica 18 bold italic")
        self.lbl_pane_title.grid(row=0, column=0, columnspan=2, sticky="nsew")
        self.lbl_pane_subtitle = tk.Label(self.frm_pane, font="Helvetica 15 bold italic")
        self.lbl_pane_subtitle.grid(row=1, column=0, columnspan=2, sticky="nsew")

        self.cvs_chart = tk.Canvas(self.frm_pane, width=210, height=210)
        self.cvs_chart.grid(row=2, column=0, sticky="e")

        self.frm_key = tk.Frame(self.frm_pane)
        self.frm_key.grid(row=2, column=1, sticky="w")

        lbl_modules = tk.Label(self.frm_modules, text="Modules", font=50, anchor="w")
        lbl_modules.grid(row=0, column=0, columnspan=2, sticky="nsew", pady=(30, 0))
        lbl_overview = tk.Label(self.frm_modules, text="Overview", font=50, anchor="w")
        lbl_overview.grid(row=0, column=1, sticky="nsew", pady=(30, 0))

    # fill the frame with user specific data (called every time data is changed or this view is raised)
    def user_data(self, data):
        modules = Module.get_modules(data.id)                   # maybe change

        # destroy and recreate all the current elements to keep information up to date
        self.cvs_chart.delete("all")
        for s in self.frm_modulelist.viewPort.pack_slaves():
            s.destroy()
        for s in self.frm_key.grid_slaves():
            s.destroy()

        self.lbl_pane_title.config(text=data.name)
        self.lbl_pane_subtitle.config(text="Year " + data.year)

        start = 90
        # for all the modules that exist, draw them
        for d in modules:
            btn_course = tk.Button(self.frm_modulelist, text=d.name, anchor="w",
                                   command=lambda module_id=d.id:
                                   self.controller.module_details(module_id))
            btn_course.pack(in_=self.frm_modulelist.viewPort, fill="x")

            box_key = tk.Label(self.frm_key, text="■", fg=colours[modules.index(d)], anchor="w")
            lbl_key = tk.Label(self.frm_key, text=d.name, anchor="w")
            box_key.grid(row=modules.index(d), column=0, sticky="w")
            lbl_key.grid(row=modules.index(d), column=1, sticky="w")

            seg = self.cvs_chart.create_arc(10, 10, 200, 200, start=start, activewidth=3,
                                            extent=d.weight*3, fill=colours[modules.index(d)])
            self.cvs_chart.tag_bind(seg, "<Button-1>", lambda e, module_id=d.id:
                                    self.controller.module_details(module_id))
            start = (start + int(d.weight*3)) % 360

        btn_logout = tk.Button(self, text="Logout", width=6,
                               command=lambda: self.controller.logout(self.__class__.__name__))
        btn_logout.grid(row=0, column=2, sticky="ne")


# drawing the modules view
class Modules(tk.Frame):
    # initialisation
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

        # drawing the tkinter modules which make up the view
        self.frm_title = tk.Frame(self)
        self.frm_title.grid(column=1, sticky="nsew")

        btn_back = tk.Button(self, text="Back", width=6,
                             command=lambda: controller.show_frame("Dashboard"))
        btn_back.grid(row=0, sticky="nw")

        self.frm_assessments = tk.Frame(self)
        self.frm_assessments.grid(row=1, column=1, sticky="nsew")
        self.frm_assessments.columnconfigure(1, weight=1)
        self.frm_assessments.rowconfigure(1, weight=1)

        self.frm_pane = tk.Frame(self.frm_assessments, highlightbackground="black",
                                 highlightcolor="black", highlightthickness=1)
        self.frm_pane.grid(row=1, column=1, sticky="nsew")
        self.frm_pane.rowconfigure(2, weight=1)
        self.frm_pane.columnconfigure(1, weight=1)

        self.lbl_pane_title = tk.Label(self.frm_pane, font="Helvetica 15 bold italic", text="Overview")
        self.lbl_pane_title.grid(row=0, column=1, sticky="nsew")
        self.lbl_pane_subtitle = tk.Label(self.frm_pane, font="Helvetica 10 bold italic")
        self.lbl_pane_subtitle.grid(row=1, column=1, sticky="nsew")

        lbl_future = tk.Label(self.frm_pane, text="this area will contain each of this modules"
                                                  "\n assessments, their progress and their deadlines")
        lbl_future.grid(row=1, column=1, sticky="nsew")

        lbl_assessmentlist = tk.Label(self.frm_assessments, text="Assessments", font=50, anchor="w")
        lbl_assessmentlist.grid(row=0, column=0, columnspan=2, sticky="nsew", pady=(30, 0))
        lbl_details = tk.Label(self.frm_assessments, text="Overview", font=50, anchor="w")
        lbl_details.grid(row=0, column=1, sticky="nsew", pady=(30, 0))

    # writing the titles to the screen
    def title(self, title):
        for s in self.frm_title.pack_slaves():
            s.destroy()
        lbl_title = tk.Label(self.frm_title, text=title, font=self.controller.title_font)
        lbl_title.pack(side="top", fill="x")

    # fill the frame with user specific data (called every time data is changed or this view is raised)
    def user_data(self, data):
        # when there is data
        if data is not None:
            # destroy and recreate all the current elements to keep information up to date
            for s in self.frm_assessments.pack_slaves():
                s.destroy()

            frm_assessmentlist = ScrollFrame(self.frm_assessments, 210)
            frm_assessmentlist.grid(row=1, column=0, sticky="nsew")

            # drawing each of the assessments
            for d in data:
                btn_assessment = tk.Button(frm_assessmentlist, text=(d.weight+" : "+d.name), anchor="w",
                                           command=lambda assessment_id=d.id:
                                           self.controller.assessment_details(assessment_id))
                btn_assessment.pack(in_=frm_assessmentlist.viewPort, fill="x")

            btn_logout = tk.Button(self, text="Logout", width=6,
                                   command=lambda: self.controller.logout(self.__class__.__name__))
            btn_logout.grid(row=0, column=2, sticky="ne")


# drawing the assessments view
class Assessments(tk.Frame):
    # construction
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.parent = parent
        self.data = None
        self.assessment_id = None
        self.task_id = None

        # drawing the tkinter modules which make up the view
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

        self.frm_title = tk.Frame(self)
        self.frm_title.grid(column=1, sticky="nsew")

        self.btn_back = tk.Button(self, text="Back", width=6,
                                  command=lambda: controller.show_frame("Modules"))
        self.btn_back.grid(row=0, sticky="nw")

        self.frm_tasks = tk.Frame(self)
        self.frm_tasks.grid(row=1, column=1, sticky="nsew")
        self.frm_tasks.columnconfigure(1, weight=1)
        self.frm_tasks.rowconfigure(1, weight=1)

        self.frm_pane = tk.Frame(self.frm_tasks, highlightbackground="black",
                                 highlightcolor="black", highlightthickness=1)
        self.frm_pane.grid(row=1, column=1, sticky="nsew")
        self.frm_pane.rowconfigure(2, weight=1)
        self.frm_pane.columnconfigure(1, weight=1)

        self.lbl_pane_title = tk.Label(self.frm_pane, font="Helvetica 15 bold italic")
        self.lbl_pane_title.grid(row=0, column=0, columnspan=3, sticky="nsew")
        self.lbl_pane_subtitle = tk.Label(self.frm_pane, font="Helvetica 10 bold italic")
        self.lbl_pane_subtitle.grid(row=1, column=0, columnspan=3, sticky="nsew")
        self.lbl_pane_weight = tk.Label(self.frm_pane, font="Helvetica 15 bold italic")
        self.lbl_pane_weight.place(relx=1, x=-2, y=2, anchor="ne")
        self.lbl_pane_part = tk.Label(self.frm_pane, font="Helvetica 15 bold italic")
        self.lbl_pane_part.place(relx=0, x=2, y=2, anchor="nw")
        self.btn_pane_activity = tk.Button(self.frm_pane, text="Add Activity", width=10)
        self.btn_pane_overview = tk.Button(self.frm_pane, text="Overview", width=10)

        self.cvs_chart = tk.Canvas(self.frm_pane)
        self.cvs_chart.grid(row=2, column=0, columnspan=3, sticky="nsew")

        self.frm_tasklist = ScrollFrame(self.frm_tasks, 210)
        self.frm_tasklist.grid(row=1, column=0, sticky="nsew")

        lbl_tasklist = tk.Label(self.frm_tasks, text="Tasks", font=50, anchor="w")
        lbl_tasklist.grid(row=0, column=0, columnspan=2, sticky="nsew", pady=(30, 0))
        lbl_details = tk.Label(self.frm_tasks, text="Details", font=50, anchor="w")
        lbl_details.grid(row=0, column=1, sticky="nsew", pady=(30, 0))

    # writing the titles
    def title(self, title):
        for s in self.frm_title.pack_slaves():
            s.destroy()
        lbl_title = tk.Label(self.frm_title, text=title, font=self.controller.title_font)
        lbl_title.pack(side="top", fill="x")

    # fill the frame with user specific data (called every time data is changed or this view is raised)
    def user_data(self, data=None):
        if data is not None:
            self.data = data
            # destroy and recreate all the current elements to keep information up to date
            for s in self.frm_tasks.slaves():
                s.destroy()

            self.data.sort(key=attrgetter('part'))      # sort task by part number ie. 1.0, 1.1, 2.0, 2.1, 3.0
            self.tasks()

            if data:
                self.overview_callback()
                self.btn_pane_overview.config(command=lambda: self.overview_callback())
            else:
                self.lbl_pane_title.config(text="No Tasks")
                self.lbl_pane_subtitle.config(text="")
                lbl_empty = tk.Label(self.frm_tasklist, text="No Tasks", bg="gray70")
                lbl_empty.pack(in_=self.frm_tasklist.viewPort, fill="x")
                self.btn_pane_activity.grid_forget()
                self.btn_pane_overview.grid_forget()
                self.cvs_chart.delete("all")
                self.lbl_pane_weight.config(text="")
                self.lbl_pane_part.config(text="")

            btn_logout = tk.Button(self, text="Logout", width=6,
                                   command=lambda: self.controller.logout(self.__class__.__name__))
            btn_logout.grid(row=0, column=2, sticky="ne")

    def tasks(self):
        # removing each of them so there is no overlap
        for s in self.frm_tasklist.viewPort.slaves():
            s.destroy()

        def task_callback(task_id):
            self.task_id = task_id
            task_details = []
            for d in self.data:
                if d.id == task_id:
                    task_details = d
                    break

            self.cvs_chart.delete("all")
            seg1 = self.cvs_chart.create_arc(10, 10, 200, 200, start=180, extent=-180, fill="red")
            self.cvs_chart.move(seg1, 20, 60)
            lbl_progress = self.cvs_chart.create_text(10, 200, text="Not started")
            self.cvs_chart.move(lbl_progress, 115, -25)

            self.lbl_pane_title.config(text=task_details.name)
            self.lbl_pane_subtitle.config(text="Not Started")
            self.lbl_pane_weight.config(text=task_details.weight+"%")
            self.lbl_pane_part.config(text=task_details.part)

            self.btn_pane_activity.config(state="normal", command=lambda:
                                          self.controller.activities(self.data, task_id))

            if task_details.progress is None:
                pass
            elif task_details.progress == "100":
                self.cvs_chart.itemconfig(seg1, fill="green")
                self.cvs_chart.itemconfig(lbl_progress, text="Complete")
                self.lbl_pane_subtitle.config(text="Complete")
                # self.btn_pane_activity.config(state="disabled")
            elif 0 < int(task_details.progress) < 100:
                seg2 = self.cvs_chart.create_arc(10, 10, 200, 200, start=180, fill="green",
                                                 extent=-((180/100)*int(task_details.progress)))
                self.cvs_chart.move(seg2, 20, 60)
                self.cvs_chart.itemconfig(lbl_progress, text=task_details.progress+"% complete")
                self.lbl_pane_subtitle.config(text="In Progress")

            self.cvs_chart.grid(row=2, column=0, columnspan=1, sticky="w")
            self.btn_pane_overview.config(state="normal")
            self.btn_pane_activity.grid(row=3, column=2, sticky="e", padx=(0, 10), pady=(0, 10))
            self.btn_pane_overview.grid(row=3, column=0, sticky="w", padx=(10, 0), pady=(0, 10))

        btn_add_task = tk.Button(self.frm_tasklist, text="Add/Remove Task", bg="sky blue", anchor="w",
                                 command=lambda: self.controller.tasks(self.assessment_id))
        btn_add_task.pack(in_=self.frm_tasklist.viewPort, fill="x")

        # putting in each of the task buttons
        for d in self.data:
            btn_task = tk.Button(self.frm_tasklist, text=d.name, anchor="w", bg="red",
                                 command=lambda task_id=d.id: task_callback(task_id))
            if d.progress is None:
                pass
            elif d.progress == "100":
                btn_task.config(bg="green")
            elif 0 < int(d.progress) < 100:
                btn_task.config(bg="orange")

            btn_task.pack(fill="x", in_=self.frm_tasklist.viewPort)

    def overview_callback(self):
        self.lbl_pane_title.config(text="Overview")
        # set subtitle as percentage progress here
        self.lbl_pane_subtitle.config(text="x% Complete")
        self.lbl_pane_weight.config(text="")
        self.lbl_pane_part.config(text="")
        self.cvs_chart.grid(row=2, column=0, columnspan=3, sticky="nsew")
        self.cvs_chart.delete("all")
        self.cvs_chart.create_text(275, 165, text="Gantt chart will go here")
        self.btn_pane_activity.grid_forget()
        self.btn_pane_overview.grid_forget()

    def dim(self):
        # bind all widgets on page behind the modal to dismiss the modal
        self.parent.bind("<Button-1>", lambda e: self.undim())
        self.bind_class("Frame", "<Button-1>", lambda e: self.undim())
        self.unbind_class("Activity", "<Button-1>")
        self.bind_class("Label", "<Button-1>", lambda e: self.undim())
        self.cvs_chart.bind("<Button-1>", lambda e: self.undim())
        self.frm_tasklist.canvas.bind("<Button-1>", lambda e: self.undim())
        self.btn_pane_activity.config(command=lambda: self.undim())
        self.btn_pane_overview.config(command=lambda: self.undim())
        for s in self.frm_tasklist.viewPort.slaves():
            if s.__class__.__name__ == "Button":
                s.config(command=lambda: self.undim())

    def undim(self):
        # unbind/rebind all the widgets on the page behind the modal
        self.parent.unbind("<Button-1>")
        self.unbind_class("Frame", "<Button-1>")
        self.unbind_class("Label", "<Button-1>")
        self.cvs_chart.unbind("<Button-1>")
        self.frm_tasklist.canvas.unbind("<Button-1>")
        self.btn_pane_overview.config(command=lambda: self.overview_callback())
        self.btn_pane_activity.config(state="normal", command=lambda:
                                      self.controller.activities(self.data, self.task_id))

        self.controller.assessment_details(self.assessment_id)


# drawing the activities screen
class Activities(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, width=400, height=450, highlightbackground="black",
                          highlightcolor="black", highlightthickness=1, bg="floral white")
        self.controller = controller
        self.task_id = None

        # drawing the tkinter modules which make up the view
        self.container = tk.Frame(self, bg="floral white")
        self.container.pack(side="top", fill="both", expand=True, pady=(5, 5), padx=(70, 70))
        self.container.columnconfigure(1, weight=1)

        self.lbl_title = tk.Label(self.container, text="Add Activity", font="Helvetica 15 bold italic",
                                  bg="floral white")
        self.lbl_title.grid(row=0, column=0, columnspan=2, sticky="nsew", pady=(5, 5))
        self.lbl_task = tk.Label(self.container, bg="floral white")
        self.lbl_task.grid(row=1, column=0, columnspan=2, sticky="ew")

        self.lbl_activities = tk.Label(self.container, text="Past Activities:", bg="floral white")
        self.lbl_activities.grid(row=2, column=0, columnspan=2, sticky="w")
        self.frm_activities = ScrollFrame(self.container, 236, 120)
        self.frm_activities.grid(row=3, column=0, columnspan=2, sticky="nsew", pady=(0, 30))
        self.frm_activities.viewPort.columnconfigure(0, weight=1)

        self.lbl_description = tk.Label(self.container, text="Description: ", bg="floral white")
        self.lbl_description.grid(row=4, column=0, sticky="e", pady=(0, 5))

        self.lbl_progress = tk.Label(self.container, text="Progress: ", bg="floral white")
        self.lbl_progress.grid(row=5, column=0, sticky="e", pady=(0, 5))

        self.lbl_duration = tk.Label(self.container, text="Duration: ", bg="floral white")
        self.lbl_duration.grid(row=6, column=0, sticky="e", pady=(0, 30))

        unbind_tags(self)           # prevent lables and frames inside the modal screen from dismissing it
        unbind_tags(self.container)
        unbind_tags(self.frm_activities)
        for c in self.container.children.values():
            if c.__class__.__name__ == "Label":
                unbind_tags(c)

    # fill the frame with user specific data (called every time data is changed or this view is raised)
    def user_data(self, data):
        for s in self.frm_activities.viewPort.grid_slaves():
            s.destroy()

        # setting activities
        activities = None
        if data:
            # maybe change
            activities = Activity.get_activities(data.id, self.controller.user.id)
            self.lbl_task.config(text=data.name)

        # refilling activities, where present
        self.frm_activities.canvas.yview_moveto(0)  # setting the scrollbar to the top
        if activities:                              # if there are activities associated with the current user
            rowNo = 0
            for a in activities:
                lbl_activity = tk.Label(self.frm_activities.viewPort, anchor="w",
                                        text=a.progress+"%\t"+a.name, bg="gray70")
                lbl_activity.grid(row=rowNo, column=0, sticky="ew")
                btn_remove = SquareButton(self.frm_activities.viewPort, text="x", bg="red", size=10,
                                          command=lambda activity=a: self.controller.remove_activity(activity))
                btn_remove.grid(row=rowNo, column=1, sticky="nsew")
                unbind_tags(lbl_activity)
                rowNo += 1
        else:                                       # no activities associated with the current user
            lbl_empty = tk.Label(self.frm_activities.viewPort, text="No Activities", bg="gray70")
            lbl_empty.grid(row=0, column=0, columnspan=2, sticky="ew")
            unbind_tags(lbl_empty)

        ent_description = tk.Entry(self.container, bg="gray94")
        ent_description.grid(row=4, column=1, sticky="ew", pady=(0, 5))

        # define the available progress options for the progress spinbox (from current task progress - 100)
        base_progress = 0
        values = []
        for a in activities:
            if int(a.progress) > base_progress:
                base_progress = int(a.progress)
        for i in range(100, base_progress, -5):
            values.append(i)
        values.reverse()
        spn_progress = tk.Spinbox(self.container, state="readonly", values=values)
        spn_progress.grid(row=5, column=1, sticky="w", pady=(0, 5))

        # define the available values for the duration spinbox (0:10, 0:20 ... 2:00, 2:10 format)
        values = []
        hours = 0
        for i in range(10, 1200, 10):
            minutes = i % 60
            if minutes == 0:
                hours += 1
            values.append(str(hours)+":"+str(minutes))
        spn_duration = tk.Spinbox(self.container, values=values, state="readonly")
        spn_duration.grid(row=6, column=1, sticky="w", pady=(0, 30))

        # button callback to grab the information and create an object to send to the controller
        def add_callback():
            desc = ent_description.get()
            prog = spn_progress.get()
            dura = spn_duration.get()
            new_activity = Activity("id", self.controller.user.id,
                                    self.task_id, desc, dura, prog)

            self.controller.add_activity(new_activity)

        btn_add = tk.Button(self.container, text="Add", width=10,
                            command=lambda: add_callback())
        if data.progress == "100":                      # if the task is already complete disable the add task button
            btn_add.config(state="disabled")

        btn_add.grid(row=7, column=0, columnspan=2)


# drawing the tasks screen
class Tasks(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent, width=400, height=450, highlightbackground="black",
                          highlightcolor="black", highlightthickness=1, bg="floral white")
        self.controller = controller
        self.assessment_id = None

        # drawing the tkinter modules which make up the view
        self.container = tk.Frame(self, bg="floral white")
        self.container.pack(side="top", fill="both", expand=True, pady=(5, 5), padx=(70, 70))
        self.container.columnconfigure(1, weight=1)

        self.lbl_title = tk.Label(self.container, text="Add/Remove Tasks", font="Helvetica 15 bold italic",
                                  bg="floral white")
        self.lbl_title.grid(row=0, column=0, columnspan=2, sticky="nsew", pady=(5, 5))
        self.lbl_task = tk.Label(self.container, bg="floral white")
        self.lbl_task.grid(row=1, column=0, columnspan=2, sticky="ew")

        self.lbl_tasks = tk.Label(self.container, text="Tasks:", bg="floral white")
        self.lbl_tasks.grid(row=2, column=0, columnspan=2, sticky="w")
        self.frm_tasks = ScrollFrame(self.container, 236, 80)
        self.frm_tasks.grid(row=3, column=0, columnspan=2, sticky="nsew", pady=(0, 20))
        self.frm_tasks.viewPort.columnconfigure(0, weight=1)

        self.lbl_description = tk.Label(self.container, text="Name: ", bg="floral white")
        self.lbl_description.grid(row=4, column=0, sticky="e", pady=(0, 5))

        self.lbl_partno = tk.Label(self.container, text="Part No. ", bg="floral white")
        self.lbl_partno.grid(row=5, column=0, sticky="e", pady=(0, 5))

        self.lbl_type = tk.Label(self.container, text="Type: ", bg="floral white")
        self.lbl_type.grid(row=6, column=0, sticky="e", pady=(0, 5))

        self.lbl_description = tk.Label(self.container, text="Description: ", bg="floral white")
        self.lbl_description.grid(row=7, column=0, sticky="ne", pady=(0, 5))

        self.lbl_weight = tk.Label(self.container, text="Weight: ", bg="floral white")
        self.lbl_weight.grid(row=8, column=0, sticky="e", pady=(0, 30))

        # prevent frame and label modules inside the modal screen from dismissing it because of the global module binds
        # in the assessment.dim() method. This removes the tag binds for the modules on this view so that they aren't
        # rebound in that method.
        unbind_tags(self)
        unbind_tags(self.container)
        unbind_tags(self.frm_tasks)
        for c in self.container.children.values():
            if c.__class__.__name__ == "Label":
                unbind_tags(c)

    # fill the frame with user specific data (called every time data is changed or this view is raised)
    def user_data(self, data):
        for s in self.frm_tasks.viewPort.grid_slaves():
            s.destroy()

        tasks = Task.get_tasks(self.assessment_id, self.controller.user)        # maybe change

        tasks.sort(key=attrgetter('part'))      # sort task by part number ie. 1.0, 1.1, 2.0, 2.1, 3.0
        self.frm_tasks.canvas.yview_moveto(0)   # set scrollbar to top
        if tasks:                               # if there are tasks for this user
            rowNo = 0
            for t in tasks:
                lbl_activity = tk.Label(self.frm_tasks.viewPort, anchor="w",
                                        text=t.weight+"%\t"+t.name, bg="gray70")
                lbl_activity.grid(row=rowNo, column=0, sticky="ew")
                btn_remove = SquareButton(self.frm_tasks.viewPort, text="x", bg="red", size=10,
                                          command=lambda task=t: self.controller.remove_task(task))
                btn_remove.grid(row=rowNo, column=1, sticky="nsew")
                unbind_tags(lbl_activity)
                rowNo += 1
        else:                                   # no tasks for the current user
            lbl_empty = tk.Label(self.frm_tasks.viewPort, text="No Tasks", bg="gray70")
            lbl_empty.grid(row=0, column=0, sticky="ew")
            unbind_tags(lbl_empty)

        ent_name = tk.Entry(self.container, bg="gray94")
        ent_name.grid(row=4, column=1, sticky="ew", pady=(0, 5))

        frm_partno = tk.Frame(self.container)
        spn_partno1 = tk.Spinbox(frm_partno, width=2, from_=1, to=100, increment=1)
        lbl_point = tk.Label(frm_partno, text=".", bg="floral white")
        spn_partno2 = tk.Spinbox(frm_partno, width=2, from_=0, to=100, increment=1)
        spn_partno1.pack(side="left")
        lbl_point.pack(side="left")
        spn_partno2.pack(side="left")
        frm_partno.grid(row=5, column=1, sticky="w", pady=(0, 5))

        opt_type = ttk.Combobox(self.container, values=["Revision", "Programming", "Writing", "Reading", "Other"],
                                state="readonly")
        opt_type.current(0)
        opt_type.grid(row=6, column=1, sticky="w", pady=(0, 5))

        ent_description = ScrolledText(self.container, bg="gray94", height=3)
        ent_description.grid(row=7, column=1, sticky="ew", pady=(0, 5))

        # button callback to grab the information and create an object to send to the controller
        def add_callback():
            name = ent_name.get()
            part = spn_partno1.get() + "." + spn_partno2.get()
            desc = ent_description.get(1.0, "end")
            desc = desc.replace("\n", " ")
            type = opt_type.get()
            weight = spn_weight.get()
            new_task = Task("id", self.assessment_id, self.controller.user.id,
                            part, name, type, desc, weight, "0")

            self.controller.add_task(new_task)

        btn_add = tk.Button(self.container, text="Add", width=10,
                            command=lambda: add_callback())
        btn_add.grid(row=9, column=0, columnspan=2)

        total_weight = 100
        values = []
        for t in tasks:
            total_weight -= int(t.weight)
        for i in range(5, total_weight+1, 5):
            values.append(i)
        if not values:
            btn_add.config(state="disabled")

        spn_weight = tk.Spinbox(self.container, state="readonly", values=values)
        spn_weight.grid(row=8, column=1, sticky="w", pady=(0, 30))


# being able to make a scroll bar where there is more information than can fit in a frame
class ScrollFrame(tk.Frame):
    def __init__(self, parent, width, height=None):
        super().__init__(parent)    # create a frame (self)

        self.canvas = tk.Canvas(self, borderwidth=0, width=width, height=height, bg="gray80")
        self.viewPort = tk.Frame(self.canvas)
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both")
        self.canvas.create_window((0, 0), window=self.viewPort, anchor="nw",
                                  tags="self.viewPort", width=width)

        self.viewPort.bind("<Configure>", self.on_frame_configure)

    def on_frame_configure(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))


# helper class to create a square shaped button for the add/remove tasks/activities modals
class SquareButton(tk.Button):
    """A new type of Button that is a square with the sides :side: pixels long"""
    def __init__(self, master=None, size=None, **kwargs):
        self.img = tk.PhotoImage()
        tk.Button.__init__(self, master, image=self.img, compound=tk.CENTER, width=size, height=size, **kwargs)
