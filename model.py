# model.py

# necessary importing
import csv
import os.path

# path to the stored data
ROOT = os.path.dirname(__file__)
new = os.path.join(ROOT, "./data/new")

# creating the current user
class User:
    # setting the exact path
    path = os.path.join(ROOT, "./data/users")

    # constructor
    def __init__(self, uid, username, password, name, year, school):
        # reassigning the info locally so if the constructor changes, behaviour remains
        self.id = uid
        self.username = username
        self.password = password
        self.name = name
        self.year = year
        self.school = school

    # this will return one user object based on the username and password put in earlier
    @classmethod
    def get_user(cls, username, password):
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[1] == username:
                    if row[2] == password:
                        return User(*row)
        f.close()
        return -1

# creating each of the modules as an object
class Module:
    # setting the correct file path
    path = os.path.join(ROOT, "./data/modules")
    enrolments_path = os.path.join(ROOT, "./data/enrolments")

    # constructor
    def __init__(self, id, name, semester, weight):
        # reassigning locally as in user
        self.id = id
        self.name = name
        self.semester = semester
        self.weight = weight

    # the method for getting them from the user CSV, making a list of Module objects
    @classmethod
    def get_modules(cls, uid):
        enrolments = []
        # getting from the database
        with open(cls.enrolments_path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[0] == uid:
                    enrolments.append(row[1])
        f.close()

        # transforming them into module objects
        modules = []
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[0] in enrolments:
                    modules.append(Module(*row))
        f.close()
        return modules

    # getting the modules from their module id
    @classmethod
    def get_module(cls, module_id):
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[0] == module_id:
                    return Module(*row)
        f.close()
        return -1


class Assessment:
    # setting the correct file path again
    path = os.path.join(ROOT, "./data/assessments")

    # constructor
    def __init__(self, id, module_id, name, due_date, weight):
        self.id = id
        self.module_id = module_id
        self.name = name
        self.due_date = due_date
        self.weight = weight

    # getting the assessments which come from a module in the csv
    @classmethod
    def get_assessments(cls, module_id):
        assessments = []
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[1] == module_id:
                    assessments.append(Assessment(*row))
        return assessments

    # getting assessments from their own id
    @classmethod
    def get_assessment(cls, assessment_id):
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",")
            for row in reader:
                if row[0] == assessment_id:
                    return Assessment(*row)
        f.close()
        return -1


class Task:
    # setting the right paths
    path = os.path.join(ROOT, "./data/tasks")

    def __init__(self, id, assessment_id, user_id, part, name, type, description, weight, progress):
        # reassigning locally as above
        self.id = id
        self.assessment_id = assessment_id
        self.user_id = user_id
        self.part = part
        self.name = name
        self.type = type
        self.description = description
        self.weight = weight
        self.progress = progress

    # loading in a list of task objects from their assessment
    @classmethod
    def get_tasks(cls, assessment_id, user):
        tasks = []
        with open(cls.path, newline="") as f:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            for row in reader:
                if row[1] == assessment_id and row[2] == user.id:
                    task = Task(*row)
                    tasks.append(task)
        f.close()
        return tasks

    @classmethod
    def next_id(cls):
        if os.path.exists(cls.path) and os.path.getsize(cls.path) > 0:
            with open(cls.path, newline="") as f:
                id = list(f)[-1].split(",")[0]
                new_id = int(id) + 1
                return "%02d" % (new_id,)
        else:
            return "00"

    # adds a new activity and associates it with a task
    @classmethod
    def add_task(cls, task):
        with open(cls.path, "a") as f:
            f.write(task.id + "," + task.assessment_id + "," + task.user_id + "," +
                    task.part + ',"' + task.name + '",' + task.type + ',"' + task.description
                    + '",' + task.weight + "," + task.progress + "\n")

    # changing the progress when they have done more work on it
    @classmethod
    def update_progress(cls, user_id, task_id, progress):
        with open(cls.path, newline="") as f, \
                open(new, "w", newline="") as out:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            writer = csv.writer(out)
            for row in reader:
                if row[2] == user_id and row[0] == task_id:
                    row[8] = progress
                    writer.writerow(row)
                else:
                    writer.writerow(row)
        os.remove(cls.path)
        os.rename(new, cls.path)

    @classmethod
    def remove_task(cls, task_id):
        with open(cls.path, newline="") as f, \
                open(new, "w", newline="") as out:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            writer = csv.writer(out)
            for row in reader:
                if row[0] != task_id:
                    writer.writerow(row)
        os.remove(cls.path)
        os.rename(new, cls.path)

        Activity.remove_task(task_id)


class Activity:
    # making the path to the csv
    path = os.path.join(ROOT, "./data/activities")

    def __init__(self, id, user_id,task_id, name, duration, progress):
        # reassigning for flexability
        self.id = id
        self.user_id = user_id
        self.task_id = task_id
        self.name = name
        self.duration = duration
        self.progress = progress

    # getting a list of activities based on the task and the user
    @classmethod
    def get_activities(cls, task_id, user_id):
        activities = []
        with (open(cls.path, newline="")) as f:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            for row in reader:
                if row[1] == user_id:
                    if row[2] == task_id:
                        activities.append(Activity(*row))
        return activities

    # ensures when you add a line it will use a unique key
    @classmethod
    def next_id(cls):
        if os.path.exists(cls.path) and os.path.getsize(cls.path) > 0:
            with open(cls.path, newline="") as f:
                id = list(f)[-1].split(",")[0]
                new_id = int(id) + 1
                return "%02d" % (new_id,)
        else:
            return "00"

    # adds a new activity and associates it with a task
    @classmethod
    def add_activity(cls, activity):
        with open(cls.path, "a") as f:
            f.write(activity.id+","+activity.user_id+","+activity.task_id+',"'+
                    activity.name+'",'+activity.duration+","+activity.progress+"\n")

        Task.update_progress(activity.user_id, activity.task_id, activity.progress)

    @classmethod
    def remove_task(cls, task_id):
        with open(cls.path, newline="") as f, \
                open(new, "w", newline="") as out:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            writer = csv.writer(out)
            for row in reader:
                if row[2] != task_id:
                    writer.writerow(row)
        f.close()
        out.close()
        os.remove(cls.path)
        os.rename(new, cls.path)

    # doesn't currently do anything because editing csv files is very long winded
    @classmethod
    def remove_activity(cls, activity_id):
        with open(cls.path, newline="") as f, \
                open(new, "w", newline="") as out:
            reader = csv.reader(f, delimiter=",", quotechar='"')
            writer = csv.writer(out)
            for row in reader:
                if row[0] != activity_id:
                    writer.writerow(row)
        f.close()
        out.close()
        os.remove(cls.path)
        os.rename(new, cls.path)


# class Milestone:
#
#

